const mongoose = require('mongoose')

const PostSchema = new mongoose.Schema({
    title : String,
    body : String,
    ncurtidas : Number,
    autor : String
})

module.exports = mongoose.model('Post', PostSchema)
