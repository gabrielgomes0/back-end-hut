const express = require('express');
const Postcontroller = require('./controllers/Postcontroller')

const routes = express.Router();

routes.get('/users', Postcontroller.index)
    //return res.json({idade : req.query.idade})
    

routes.post('/users', Postcontroller.store)
    //return res.json({ message : 'Opa'})
    //return res.json(req.body)


routes.post('/users/:id', Postcontroller.update)


routes.delete('/users/:id', Postcontroller.destroy)


module.exports = routes;