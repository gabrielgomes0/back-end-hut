const Post = require('../models/Post')

module.exports = {
    async index(req, res) {
        post = await Post.find()
        return res.json(post)
    },
    async update(req, res) {
        const id = req.params.id
        
        const post = await Post.findByIdAndUpdate(id,req.body)
        if(!post){
            return res.json({ message : 'Nenhum post com esse titulo foi encontrado.'})
        }
        return res.json({ message : 'Post alterado com sucesso!'})

    },
    async destroy(req, res) {
        const id = req.params.id

        const post = await Post.findByIdAndDelete(id)
        if(!post){
            return res.json({ message : 'Nenhum post com esse titulo foi encontrado.'})
        }
        return res.json({message : 'Post apagado com sucesso!'})
    },
    
    async store(req, res) {
        const { title,body,ncurtidas,autor } = req.body
        
        if(!autor){
            return res.status(400).json('Ta faltando autor parça.')
        }

        const post = await Post.create({ title, body, ncurtidas, autor })

        return res.json(post)
    }
}