const express = require('express')
const mongoose = require('mongoose')
const routes = require('./routes')

const app = express()
require('dotenv/config')

mongoose.connect(process.env.DB_USE, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})


app.use(express.json())
app.use(routes)


app.listen(3333)
